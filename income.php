<?php
	session_start();
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}
	
?>
<html>
	<head>
		<title>One Agno Accounting Department</title>
		<style>
		body{ background-image: url(img/15.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;}
				.bodyform table input{width:100%;height:40px;}
				.bodyform table h2{color:green;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13px arial;}
				.bodyform input:hover{background-color:white;color:green;}
			
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
			
			.schedform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.schedform table{width:100%;}
				.schedform table th{background-color:green;color:white;height:40px;}
				.schedform table tr td{background-color:white;}
				.schedform table button{width:100%;background-color:green;color:white;border:0px;}
				
			.overdueholder{width:100%;margin:auto auto;position:relative;padding-top:10px;max-height:350px;}
				.overdueholder table{width:100%;color:black;border-collapse:collapse;}
				.overdueholder table input[type!=date]{width:100%;height:40px;}				
				.overdueholder table tr td{font:13pt arial;color:black;border:1px solid green}
				.overdueholder table tr:first-child td{border:0px;}
				.overdueholder table h2{color:green;}
				.overdueholder table tr th{font:15pt arial;background-color:white;height:40px;color:green;border-radius:15px 15px 0px 0px;border:1px solid green;}
				.overdueholder table input{background-color:green;color:white;font:13pt arial;}
				.overdueholder input[type=submit]{background-color:green;color:white;font:13pt arial;}
				.overdueholder button{background-color:green;color:white;font:13pt arial;width:98%;border:0px;}
            .itemSummaryDiv{width:100%;margin:auto auto;position:relative;height:600px;}
                .vitrosProdSummary{width:98%;margin:auto auto;position:relative;}
                .vitrosProdSummary table{width:98%;max-height:500px;<overflow:auto></overflow:auto>;}
				
		</style>
        <script>
            function customerSelect(selected)
            {
                var selectedInfo=selected.split('*');
                document.getElementsByName('tb_selectedCustomer')[0].value=selectedInfo[0];
                document.getElementsByName('tb_selectedCustomerAddress')[0].value=selectedInfo[1];
            }
        </script>
	</head>
	<body>
		<form method='POST'>
			<div class='container'>
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
							<td><a href='collection.php'>Collection</a></td>
							<td><a href='schedules.php'>Schedules</a></td>
							<td><a href='report.php'>Reports</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Statement of account for <?php echo $_SESSION['Customer'];?></h2></div>
			
				<div class='bodyform'>
                    <table>
                        <tr>
                            <td>
                                Name of Customer
                                <select name='cb_customer'onchange='return customerSelect(this.value);'>
									<option disabled selected>-Existing Customer List-</option>
									<?php
										$existingCustomerSQL="Select* from tbl_customer";
										$existingCustomers=$con->query($existingCustomerSQL);
										if($existingCustomers->num_rows>0)
										{
											while($ECustomer=$existingCustomers->fetch_assoc())
											{
												echo"<option value='".$ECustomer['Name']."*".$ECustomer['Address']."'>".$ECustomer['Name']."</option>";
											}
											
										}
									?>
								</select>
                            </td>
                            <td><input type='text'name='tb_selectedCustomer'></td>
                            <td><input type='text'name='tb_selectedCustomerAddress'></td>
                            <td>&nbsp;<input type='submit'name='btn_CustomerSummary'value='View Summary'></td>
                        </tr>
                    </table>
					
				</div>
                           
                <div class='itemSummaryDiv'>
                        <?php
                            if(isset($_POST['btn_CustomerSummary']))
                            {
                                    $_SESSION['Customer']=$_POST['tb_selectedCustomer'];
                            }
                        ?>
                        <div class='vitrosProdSummary'>
                                <h2>Summary for Vitros Products</h2>
                                <table>
                                        <tr>
                                            <td>Item Description</td>
                                            <td>Total Quantity Sold</td>
                                            <td>Price per Box</td>
                                            <td>Total</td>
                                        </tr>
                                        <?php
                                            echo $_SESSION['Customer'];
                                            $CustomerItemsSQL="Select* from tbl_pricelist WHERE HospitalName='".addslashes($_SESSION['Customer'])."'";
                                            $CustomerItems=$con->query($CustomerItemsSQL);
                                            if($CustomerItems->num_rows>0)
                                            {
                                                $LinkedProdSQL="";
                                                $LinkedProds="";
                                                $LinkedProd="";
                                                while($CustomerItem=$CustomerItems->fetch_assoc())
                                                {
                                                    $LinkedProdSQL="Select* from tbl_vprod WHERE Catalog='".$CustomerItem['CatNumber']."'";
                                                    $LinkedProds=$con->query($LinkedProdSQL);
                                                    if($LinkedProd->num_rows>0)
                                                    {

                                                    }
                                                    echo"<tr>
                                                            <td>
                                                                ".$CustomerItem["CatNumber"]."
                                                            </td>
                                                        </tr>";
                                                }
                                                /*$LinkedProdSQL="";
                                                $LinkedProds="";
                                                $LinkedProd="";
                                                while($CustomerItem=$CustomerItems->fetch_assoc())
                                                {
                                                    $LinkedProdSQL="Select* from tbl_vprod WHERE Catalog='".$CustomerItem['CatNumber']."'";
                                                    $LinkedProds=$con->query($LinkedProdSQL);
                                                    if($LinkedProds->num_rows>0)
                                                    {
                                                        $SIProdSQL="";
                                                        $SIProds="";
                                                        $SIProd="";
                                                        while($LinkedProd=$LinkedProds->fetch_assoc())
                                                        {
                                                            $SIProdSQL="Select SUM(Quantity) AS TQuantity,SUM(Total) AS TTotal,Max(Price) AS TPrice from tbl_siproducts where ProdDesc ='".addslashes($LinkedProd['Description'])."' AND Customer='".addslashes($_SESSION['Customer'])."'";
                                                            $SIProds=$con->query($SIProdSQL);
                                                            if($SIProds->num_rows>0)
                                                            {
                                                                while($SIProd=$SIProds->fetch_assoc())
                                                                {
                                                                    echo"
                                                                        <tr>
                                                                            <td>".$LinkedProd['Description']."</td>
                                                                            <td>".$SIProd['TQuantity']."</td>
                                                                            <td>".$SIProd['TPrice']."</td>
                                                                            <td>".$SIProd['TTotal']."</td>
                                                                        </tr>
                                                                    ";
                                                                }
                                                            }
                                            
                                                        }
                                                    }

                                                }*/
                                            }
                                        ?>
                                </table>
                        </div>
                </div>
            
                
				
			</div>
		</div>
	</body>
</html>
