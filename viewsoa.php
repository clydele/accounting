   <?php
	session_start();
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}
	
?>
<html>
	<head>
		<title>One Agno Accounting Department</title>
		<style>
		body{ background-image: url(img/15.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;}
				.bodyform table input{width:100%;height:40px;}
				.bodyform table h2{color:green;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13px arial;}
				.bodyform input:hover{background-color:white;color:green;}
			
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
			
			.schedform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.schedform table{width:100%;}
				.schedform table th{background-color:green;color:white;height:40px;}
				.schedform table tr td{background-color:white;}
				.schedform table button{width:100%;background-color:green;color:white;border:0px;}
				
			.overdueholder{width:100%;margin:auto auto;position:relative;padding-top:10px;max-height:350px;}
				.overdueholder table{width:100%;color:black;border-collapse:collapse;}
				.overdueholder table input[type!=date]{width:100%;height:40px;}				
				.overdueholder table tr td{font:13pt arial;color:black;border:1px solid green}
				.overdueholder table tr:first-child th{border:0px;}
				.overdueholder table h2{color:green;}
				.overdueholder table tr th{font:15pt arial;background-color:white;height:40px;color:green;border-radius:15px 15px 0px 0px;border:1px solid green;}
				.overdueholder table input{background-color:green;color:white;font:13pt arial;}
				.overdueholder input[type=submit]{background-color:green;color:white;font:13pt arial;}
				.overdueholder button{background-color:green;color:white;font:13pt arial;width:98%;border:0px;}
				
		</style>
	</head>
	<body>
		<form method='POST'>
			<div class='container'>
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
							<td><a href='collection.php'>Collection</a></td>
							<td><a href='schedules.php'>Schedules</a></td>
							<td><a href='report.php'>Reports</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Statement of account for <?php echo $_SESSION['Customer'];?></h2></div>
			
				<div class='overdueholder'>
					<?php
							
							echo"<table><tr><th>Invoice Number</th><th>PO Number</th><th>Amount Due</th><th>Payment Due Date</th></tr>";
							$CustomerSQL="";
							$invoiceQus="";
							$invoiceQu="";
							$dueCat="";
							$i=0;
							$TotalCash=0;
							while($i<3)
							{
								$TotalCash=0;
								if($i==0)
								{
									$CustomerSQL="Select* from tbl_invoice where  Status='Delivered' AND Cash>  0 AND  DATEDIFF(CURDATE(),PaymentDue)>=0 AND DATEDIFF(CURDATE(),PaymentDue)<=15 AND CName='".$_SESSION['Customer']."'";
									$dueCat="30 to 45 days";
								}
								else if($i==1)
								{

									$CustomerSQL="Select* from tbl_invoice where  Status='Delivered' AND Cash>0 AND  DATEDIFF(CURDATE(),PaymentDue)>=16 AND DATEDIFF(CURDATE(),PaymentDue)<=30 AND CName='".$_SESSION['Customer']."'";
									$dueCat="45 to 60 days";
								}
								else
								{
									$CustomerSQL="Select* from tbl_invoice where  Status='Delivered' AND Cash>0 AND DATEDIFF(CURDATE(),PaymentDue)>31 AND CName='".$_SESSION['Customer']."'";
									$dueCat="60 days and above";
								}
							
								$invoiceQus=$con->query($CustomerSQL);
								if($invoiceQus->num_rows>0)
								{
									echo"<tr><td colspan='4'style='background-color:green;color:white;'>".$dueCat."</td></tr>";
									while($invoiceQu=$invoiceQus->fetch_assoc())
									{
										$TotalCash+=$invoiceQu["Cash"];
										echo"
											<tr>
												<td>".$invoiceQu["SIN"]."</td>
												<td>".$invoiceQu["PON"]."</td>
												<td>".$invoiceQu["Cash"]."</td>
												<td>".$invoiceQu["PaymentDue"]."</td>
											
											</tr>
										";
										echo "<tr style='background-color:green;color:white;'><td colspan='3'></td><td style='color:white;'>Total Due: ".$TotalCash."</td>";
									}

								}
					
								$i++;
								
							
								
							}
							
						
							
							
							
							
					?>
					
				</div>
				<div>
					<?php
							$TotalDueSQL="SELECT SUM(Cash) AS TotalDue from tbl_invoice where Status ='Delivered' AND Cash>0 AND DATEDIFF(CURDATE(),PaymentDue)>=0";
							$TotalDue=$con->query($TotalDueSQL);
							if($TotalDue->num_rows>0)
							{
								$TotalDueInfo=$TotalDue->fetch_assoc();
								echo"
									<tr><td colspan='4' style='background-color:white;'>&nbsp;</td></tr>
									<tr>
										<td style='background-color:green;color:white;'>Total Amount Due: ".$TotalDueInfo["TotalDue"]."</td>
									</tr></table>";
							}
							
					?>
				</div>
				
				</div>
			</div>
		</div>
	</body>
</html>
