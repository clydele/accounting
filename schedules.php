<?php
	session_start();
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	if(isset($_POST['btn_submit']))
	{
		$Submit="INSERT into tbl_collection(CollectionOfficer,Booklet,Series,ORNumber,Remitted,Hospital,Date,Invoice,checkNum,checkAmt,checkBank)VALUES('".$_SESSION['COName']."','".addslashes($_POST['tb_BNumber'])."','".addslashes($_POST['tb_SNumber'])."','".addslashes($_POST['tb_ONumber'])."','".addslashes($_POST['tb_Remitted'])."','".addslashes($_POST['tb_Hospital'])."','".$_POST['dtp_date']."','".addslashes($_POST['tb_INumbers'])."','".addslashes($_POST['tb_CNumber'])."','".addslashes($_POST['tb_CAmount'])."','".addslashes($_POST['tb_CBank'])."')";
		if($con->query($Submit)===TRUE)
		{
			echo"<script>alert('Collection Submitted');</script>";
		}
	}
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}
	if(isset($_POST['btn_viewSoa']))
	{
		$_SESSION['Customer']=$_POST['btn_viewSoa'];
		header('Location:viewsoa.php');
	}
?>
<html>
	<head>
		<title>One Agno Accounting Department</title>
		<style>
		body{ background-image: url(img/15.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;}
				.bodyform table input{width:100%;height:40px;}
				.bodyform table h2{color:green;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13px arial;}
				.bodyform input:hover{background-color:white;color:green;}
			
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
			
			.schedform{width:90%;margin:auto auto;position:relative;padding-top:10px;}
				.schedform table{width:100%;}
				.schedform table th{background-color:green;color:white;height:30px;}
				.schedform table tr td{background-color:white;}
				.schedform table button{width:100%;background-color:green;color:white;border:0px;}
				
			.overdueholder{width:100%;margin:auto auto;position:relative;padding-top:10px;max-height:350px;}
				.overdueholder table{width:100%;color:black;border-collapse:collapse;}
				.overdueholder table input[type!=date]{width:100%;height:40px;}
				
				.overdueholder table tr td{font:13pt arial;color:black;border:1px solid green}
				.overdueholder table tr:first-child td{border:0px;}
				.overdueholder table h2{color:green;}
				.overdueholder table tr th{font:15pt arial;color:white;height:40px;}
				.overdueholder table input{background-color:green;color:white;font:13pt arial;}
				.overdueholder input[type=submit]{background-color:green;color:white;font:13pt arial;}
				.overdueholder button{background-color:green;color:white;font:13pt arial;width:98%;border:0px;}
				
		</style>
	</head>
	<body>
		<form method='POST'>
			<div class='container'>
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
							<td><a href='collection.php'>Collection</a></td>
							<td><a href='schedules.php'>Schedules</a></td>
							<td><a href='approval.php'>Invoice Approval</a></td>
							<td><a href='report.php'>Reports</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Upcoming Collection Schedules</h2></div>
			
				<div class='overdueholder'>
					<table>
						<tr>
							<th colspan='3'>
								<font color='green'>
								Start date<input type='date'name='dtp_startDate'>
								End date<input type='date'name='dtp_endfDate'>
								</font>
								<input type='submit'name='btn_searchUnpaid'value='Search Unpaid'style='background-color:green;border:0px;color:white;font:15pt arial;'>						
							</th>
						</tr>	
					</table>
				</div>
				<div class='schedform'>
					
					<table>
						<tr>
							<th>Invoice Number</th>
							<th>Customer</th>
							<th>Payment Due</th>
							<th>Amount Due</th>
							<th>Customer SOA</th>
							
						</tr>
						
						
						<?php
						if(isset($_POST['btn_searchUnpaid']))
						{
					
							$invoiceQuSQL="Select* from tbl_invoice where  Cash>0 AND Status='Delivered' AND PaymentDue>'".$_POST['dtp_startDate']."'AND PaymentDue<'".$_POST['dtp_endfDate']."' ORDER BY PaymentDue";
							$invoiceQus=$con->query($invoiceQuSQL);
							if($invoiceQus->num_rows>0)
							{
								while($invoiceQu=$invoiceQus->fetch_assoc())
								{
									echo"
										<tr>
											<td>".$invoiceQu["SIN"]."</td>
											<td>".$invoiceQu["CName"]."</td>
											<td>".$invoiceQu["PaymentDue"]."</td>
											<td>".$invoiceQu["Cash"]."</td>
											<td><button name='btn_viewSoa'value='".$invoiceQu["CName"]."'>View</button></td>
										</tr>
									";
								}
							}
									
						}
						else{
							$CustomerSQL="";
							$invoiceQus="";
							$invoiceQu="";
							$i=0;
							while($i<3)
							{
								if($i==0)
								{
									$CustomerSQL="Select* from tbl_invoice where Cash>0 AND  Status='Delivered' AND DateDiff(CURDATE(),PaymentDue)>=0 AND DateDiff(CURDATE(),PaymentDue)<=15";
									echo"<tr><td colspan='5'style='background-color:green;color:white;text-align:center;'>30 to 45 days due</td></tr>";
								}
								else if($i==1)
								{
									$CustomerSQL="Select* from tbl_invoice where Cash>0 AND  Status='Delivered' AND DateDiff(CURDATE(),PaymentDue)>=16 AND DateDiff(CURDATE(),PaymentDue)<=30";
									echo"<tr><td colspan='5'style='background-color:yellow;color:black;text-align:center;'>45 to 60 days due</td></tr>";
								}
								else
								{
									$CustomerSQL="Select* from tbl_invoice where Cash>0 AND  Status='Delivered' AND DateDiff(CURDATE(),PaymentDue)>=30";
									echo"<tr><td colspan='5'style='background-color:red;color:white;text-align:center;'>above 60 days due</td></tr>";
								}
								
								$invoiceQus=$con->query($CustomerSQL);
								if($invoiceQus->num_rows>0)
								{
									while($invoiceQu=$invoiceQus->fetch_assoc())
									{
										echo"
											<tr>
												<td>".$invoiceQu["SIN"]."</td>
												<td>".$invoiceQu["CName"]."</td>
												<td>".$invoiceQu["PaymentDue"]."</td>
												<td>".$invoiceQu["Cash"]."</td>
												<td><button name='btn_viewSoa'value='".$invoiceQu["CName"]."'>View</button></td>
											</tr>
										";
									}
								}
								else
								{
									echo"<tr><td colspan='5'>None</td></tr>";
								}
								$i++;
							}
						}
						?>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
