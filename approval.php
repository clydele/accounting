<?php
	session_start();
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}
	if(isset($_POST['btn_markApproved']))
	{
		$ApproveSIQuery="Update tbl_invoice SET Status='Created' where SIN='".$_POST['btn_markApproved']."'";
		if($con->query($ApproveSIQuery)===TRUE)
		{
			echo"<script>alert('Sales Invoice Marked as Approved')</script>";
		}
	}
	if(isset($_POST['btn_ApproveCluster']))
	{
		$ApproveSIQuery="Update tbl_invoice SET Status='Created' where CName='".$_POST['btn_ApproveCluster']."' and Status='For Approval'";
		if($con->query($ApproveSIQuery)===TRUE)
		{
			echo"<script>alert('Customer open or invoice creation')</script>";
		}
		
	}

?>
<html>
	<head>
		<title>One Agno Accounting Department</title>
		<style>
		
		body{ background-image: url(img/15.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.container button:hover{background-color:white;color:green;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:100%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;border-collapse:collapse;}
				.bodyform table input{width:100%;height:40px;}
				.bodyfrom table tr:last-child {border:1px solid black;}
				.bodyform table tr td{font:13pt arial;color:black;border:1px solid green}
				.bodyform table tr:first-child td{border:0px;}
				.bodyform table h2{color:green;}
				.bodyform table tr th{font:15pt arial;color:white;background-color:green;height:40px;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13pt arial;}
				.bodyform button{background-color:green;color:white;font:13pt arial;width:100%;border:0px;}
				.bodyform input:hover{background-color:white;color:green;}		
				
			.colStatsDiv{width:80%;position:relative;background-color:white;}
				.colStatsDiv table{width:100%;}
				.colStatsDiv table tr td{width:50%;}
				.colStatsDiv table h2{color:white;}
				.colStatsDiv table tr td{border:1px solid green;}
				.colStatsDiv table input{width:100%;height:100%;font:13pt arial;}
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
			.InvoiceViewer{height:300px;width:400px;background-color:white;position:fixed;z-index:1;top:250px;left:40%;border:4px solid green;display:none;scroll:auto;text-align:center;font:18pt arial;}
				.InvoiceViewer button{background-color:green;color:white;font:16pt arial;position:relative;width:100%:border:0px;height:100px;}
				.InvoiceViewer textarea{position:relative;margin;auto auto;width:98%;height:auto ;top:6px;}
				.InvoiceViewer table{width:98%;position:relative;margin:auto auto;top:3px;background-color:white;}
				.InvoiceViewer table tr{border:1px solid black;height:9%;}
				.InvoiceViewer table tr th{border:1px solid black;width:50%;background-color:green;color:white;font:24pt arial;}
				
			
		</style>
	</head>
	<body>
		<form method='POST'>
			<script>
				function show(clicked)
				{
				   document.getElementsByClassName("InvoiceViewer")[0].innerHTML="";
				   document.getElementsByClassName("InvoiceViewer")[0].style.display="block";
				   document.getElementsByClassName("container")[0].style.filter="blur(8px)";
				   var btnDesc=clicked.split('*');
				   var bulkapprovalbtn="<button type='submit'name='btn_ApproveCluster'value='"+btnDesc[1]+"'>Approve All Pending SI for "+btnDesc[1]+"</button>";
				   var approvalbtn= "<button type='submit'name='btn_markApproved'value='"+btnDesc[0]+"'>Approve SI</button>";
				   document.getElementsByClassName("InvoiceViewer")[0].innerHTML="<h2><font color='red'>NOTICE</font></h2>Approve SI No:<b>"+btnDesc[0]+"</b> for <b>"+btnDesc[1]+"</b> which has a total overdue amount of <b>"+btnDesc[2]+"</b>?<br><table><tr><td>"+ approvalbtn +"</td><td>"+bulkapprovalbtn+"</td></tr></table>";
				   
				   
				
				   
				}
				function hide()
				{
					document.getElementsByClassName("InvoiceViewer")[0].style.display="none";
					document.getElementsByClassName("container")[0].style.filter="blur(-8px)";
				}
			</script>
			<div class="InvoiceViewer">
				
				
			</div>
			<div class='container'>
				
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
							
							<td><a href='collection.php'>Collection</a></td>
							<td><a href='schedules.php'>Schedules</a></td>
							<td><a href='report.php'>Reports</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Invoices for Approval</h2></div>
			
				<div class='bodyform'>
						<table>
							<tr>
								<th>Invoice Number</th>
								<th>Date</th>
								<th>Customer</th>
								<th>Amount</th>
								<th>P.O. Number</th>
								<th>Approve SI</th>						
							</tr>

					
								<?php
									$PendingSIsql="Select* from tbl_invoice where Status='For Approval'";
									$PendingSIs=$con->query($PendingSIsql);
									if($PendingSIs->num_rows>0)
									{
										$SIDUESQL="";
										$SIDUE="";
										while($PendingSI=$PendingSIs->fetch_assoc())
										{
											echo"<tr>
											<td>".$PendingSI['SIN']."</td>
											<td>".$PendingSI['SIDate']."</td>
											<td>".$PendingSI['CName']."</td>
											<td>".$PendingSI['Cash']."</td>
											<td>".$PendingSI['PON']."</td>";
											$SIDUESQL="SELECT SUM(Cash) AS TotalDue FROM tbl_invoice WHERE Cname LIKE '%".$PendingSI['CName']."%' AND DATE_ADD( PaymentDue, INTERVAL 2 DAY )<CURDATE()";
											$SIDUE=$con->query($SIDUESQL);
											if($SIDUE->num_rows>0)
											{
												$CustomerDue=$SIDUE->fetch_assoc();
												echo"<td><button type='button'name='btn_confirmApprove'value='".$PendingSI['SIN']."*".$PendingSI['CName']."*".$CustomerDue['TotalDue']."'onclick='return show(this.value);'>Approve Invoice </button></td></tr>";
											}

										}
										
									}
									else
									{
										echo"<tr><td colspan='6'>No pending invoice as of the moment.</td></tr>";
									}
									  
								?>
							
				
						</table>
						
					
				</div>
			</div>
		</div>
	</body>
</html>
