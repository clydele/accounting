<?php
	session_start();
	$con=new mysqli("localhost","oams","oams@0108","db_salesrep");
	if($_SESSION['COName']=="")
	{
		header('Location:index.php');
	}
	
	if(isset($_POST['btn_submit']))
	{
		$Submit="INSERT into tbl_collection(CollectionOfficer,Booklet,Series,ORNumber,Remitted,Hospital,Date,Invoice,checkNum,checkAmt,checkBank)VALUES('".$_SESSION['COName']."','".addslashes($_POST['tb_BNumber'])."','".addslashes($_POST['tb_SNumber'])."','".addslashes($_POST['tb_ONumber'])."','".addslashes($_POST['tb_Remmited'])."','".addslashes($_POST['tb_Hospital'])."','".$_POST['dtp_date']."','".addslashes($_POST['tb_INumbers'])."','".addslashes($_POST['tb_CNumber'])."','".addslashes($_POST['tb_CAmount'])."','".addslashes($_POST['tb_CBank'])."')";
		if($con->query($Submit)===TRUE)
		{
			echo"<script>alert('Collection Submitted');</script>";
		}
	}
	if(isset($_POST['btn_logout']))
	{
		session_destroy();
		header('Location:index.php');
	}

?>
<html>
	<head>
		<title>One Agno Accounting Department</title>
		<style>
		body{ background-image: url(img/15.jpg);background-size:cover;background-repeat:inherit;}
			.container{width:1000px;height:auto;margin:auto auto;position:relative;}
			.header{width:90%;height:90px;position:relative;margin:auto auto;text-align:center;vertical-align:center;font:40pt impact;background-color:white;border-radius:15px 15px 0px 0px;}
			.navigation{width:99%;height:50px;margin:auto auto;background-color:green;padding-left:5px;padding-right:5px;}
				.navigation table{width:80%;margin:auto auto;position;relative;}
				.navigation a{height:50px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;text-decoration:none;}
				.navigation a:hover{background-color:white;color:green;}
				.navigation input{height:40px;margin:auto auto;padding-left:3px;padding-right:3px;background-color:green;color:white;font:18pt arial;border:0px;}
				.navigation input:hover{background-color:white;color:green;}
			.bodyform{width:100%;margin:auto auto;position:relative;padding-top:10px;}
				.bodyform table{width:100%;color:black;border-collapse:collapse;background-color:white;}
				.bodyform table input{width:100%;height:40px;}
				.bodyfrom table tr:last-child {border:1px solid black;}
				.bodyform table tr td{font:12pt arial;color:black;border:1px solid green}
				.bodyform table tr:first-child th{border:0px;}
				.bodyform table h2{color:green;}
				.bodyform table tr th{font:15pt arial;color:white;background-color:green;height:40px;}
				.bodyform input[type=submit]{background-color:green;color:white;font:13pt arial;}
				.bodyform button{background-color:green;color:white;font:13pt arial;width:100%;border:0px;align:center;}
				.bodyform input:hover{background-color:white;color:green;}
				
		
				
			.colStatsDiv{width:80%;position:relative;background-color:white;}
				.colStatsDiv table{width:100%;}
				.colStatsDiv table tr td{width:50%;text-align:center;font:15pt arial;color:white;}
				.colStatsDiv table h2{color:white;}
				.colStatsDiv table tr td{border:1px solid green;}
				.colStatsDiv table input{width:100%;height:100%;font:13pt arial;border:0px;}
			.pageHeader{width:80%:margin:auto auto;position:relative;text-align:center;background-color:white;height:40px;color:green;top:-20px;}
			.InvoiceViewer{height:260px;width:400px;background-color:white;position:fixed;z-index:1;top:250px;left:40%;border:4px solid green;display:none;scroll:auto;text-align:center;}
				.InvoiceViewer button{background-color:green;color:white;font:13pt arial;position:relative;}
				.InvoiceViewer textarea{position:relative;margin;auto auto;width:98%;height:250px;top:6px;}
				.InvoiceViewer table{width:98%;position:relative;margin:auto auto;border-collapse:collapse;border:1px solid black;top:3px;}
				.InvoiceViewer table tr{border:1px solid black;height:9%;}
				.InvoiceViewer table tr th{border:1px solid black;width:50%;}
				
			
		</style>
	</head>
	<body>
		<form method='POST'>
			<script>
				function show(clicked)
				{
				   document.getElementsByClassName("InvoiceViewer")[0].innerHTML="";
				   document.getElementsByClassName("InvoiceViewer")[0].style.display="block";
				   document.getElementsByClassName("container")[0].style.filter="blur(8px)";
				   var closeButton="<button type='button'onclick='return hide();'>Close</button>";
				   var paidInvoices=clicked.split(',');
				   var a=0;
				   var tableContent="";
				   var separator;
				   while(a<paidInvoices.length)
				   {
					   if(paidInvoices[a]!=null & paidInvoices[a]!=" ")
					   {
						   separator=paidInvoices[a].split('=');
						   tableContent+="<tr><td>"+separator[0]+"</td><td>"+separator[1]+"</td></tr>";
					   }	   
					   a++;
				   }
				  
				   document.getElementsByClassName("InvoiceViewer")[0].innerHTML=closeButton+"<table><tr><th>Invoice Number</th><th>Price</th></tr>"+tableContent+"</table>";
				   
				   
				
				   
				}
				function hide()
				{
					document.getElementsByClassName("InvoiceViewer")[0].style.display="none";
					document.getElementsByClassName("container")[0].style.filter="blur(-8px)";
				}
			</script>
			<div class="InvoiceViewer">
				
				
			</div>
			<div class='container'>
				
				<div class='header'><font color='blue'>One Agno</font><font color='red'> Medical Solutions</font></div>
				<div class='navigation'>
					<table>
						<tr>
							
							<td><a href='collection.php'>Collection</a></td>
							<td><a href='schedules.php'>Schedules</a></td>
							<td><a href='approval.php'>Invoice Approval</a></td>
							<td><a href='report.php'>Reports</a></td>
							<td><input type='submit'name='btn_logout'value='Logout'></td>
						</tr>
					</table>
				</div>
				<div class='pageHeader'><h2>Collection Reports</h2></div>
			
				<table style="width:100%">
					<tr>
						<td style="width:50%;">
							<h2>Overall Collection Counter</h2>
							<div class='colStatsDiv'>								
								<table>
									<tr>
										<td bgcolor='green'>Collected Amount</td>
										<td><input type='text'name='tb_OCA'value='<?php 
												$SQLColSum="SELECT SUM(checkAMT) AS SUM from tbl_collection";
												$ColSum=$con->query($SQLColSum);
												if($ColSum->num_rows>0)
												{
													$SUM=$ColSum->fetch_assoc();
													echo $SUM["SUM"];
												}
											?>'>
										</td>
									</tr>
									<tr>
										<td bgcolor='green'>To Be Collected</td>
										<td><input type='text'name='tb_OTBC'value='<?php 
												$SQLColSum="SELECT SUM(Cash) AS SUM from tbl_invoice where Status='Delivered';";
												$ColSum=$con->query($SQLColSum);
												if($ColSum->num_rows>0)
												{
													$SUM=$ColSum->fetch_assoc();
													echo $SUM["SUM"];
												}
											?>'></td>
									</tr>
									<tr>
										<td bgcolor='green'>Total</td>
										<td><input type='text'name='tb_OTotal'value='<?php 
												$SQLColSum="SELECT SUM(Cash) AS SUM from tbl_invoice";
												$ColSum=$con->query($SQLColSum);
												if($ColSum->num_rows>0)
												{
													$SUM=$ColSum->fetch_assoc();
													echo $SUM["SUM"];
												}
											?>'></td>
									</tr>
								</table>
							</div>
						</td>
						<td style="width:50%;">
							<h2>Date Specific Counter</h2>
							<div class='colStatsDiv'>								
								<table>
									<tr>
										<td bgcolor='green'>Collected Amount</td>
										<td><input type='text'name='tb_DCA'>
										</td>
									</tr>
									<tr>
										<td bgcolor='green'>To Be Collected</td>
										<td><input type='text'name='tb_DTBC'></td>
									</tr>
									<tr>
										<td bgcolor='green'>Total</td>
										<td><input type='text'name='tb_DTotal'></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
				<div class='bodyform'>
					<table id='collapsed-table'>
						<tr>
							<td>From:<input type='date'name='dtp_from'></td>
							<td>To:<input type='date'name='dtp_to'></td>
							<td>&nbsp;<input type='submit'value='Search'name='btn_searchCol'></td>
							<td>&nbsp;<input type='submit'value='Clear'></td>
						</tr>
					</table>
					<table>
						<thead>
							<tr>
								<th>Date</th>
								<th>Hospital</th>
								<th>OR Number</th>
								<th>Amount</th>
								<th>Invoices</th>
								<th>Collection Officer</th>						
							</tr>
						</thead>
						<tbody>
							<?php
								if(isset($_POST['btn_searchCol']))
								{
									
									$FirstDate=$_POST["dtp_from"];
									$SecDate=$_POST["dtp_to"];
									$SQLColRecords="Select* from tbl_collection where Date BETWEEN '".$FirstDate."' AND '".$SecDate."'";
									$ColRecords=$con->query($SQLColRecords);
									if($ColRecords->num_rows>0)
									{
										$Total="";
										while($ColRecord=$ColRecords->fetch_assoc())
										{
											echo"
											<tr>
												<td>".$ColRecord["Date"]."</td>
												<td>".$ColRecord["Hospital"]."</td>
												<td>".$ColRecord["ORNumber"]."</td>
												<td>".$ColRecord["checkAmt"]."</td>
												<td><button name='btn_Invoices'type='button'value='".$ColRecord["Invoice"]."'>Show Invoices</button></td>
												<td>".$ColRecord["CollectionOfficer"]."</td>
											</tr>
											";
											$Total+=$ColRecord["checkAmt"];
											
										}
										echo"<script>document.getElementsByName('tb_DCA')[0].value='".$Total."'</script>";
										
										$SQLColSum="SELECT SUM(Cash) AS SUM from tbl_invoice";
										$ColSum=$con->query($SQLColSum);
										if($ColSum->num_rows>0)
										{
											$SUM=$ColSum->fetch_assoc();
											echo"<script>document.getElementsByName('tb_DTBC')[0].value='".$SUM["SUM"]."'</script>";
											
										}
										$SQLColSum="SELECT SUM(Cash) AS SUM from tbl_invoice where Status='Delivered '";
										$ColSum=$con->query($SQLColSum);
										if($ColSum->num_rows>0)
										{
											$SUM=$ColSum->fetch_assoc();
											echo"<script>document.getElementsByName('tb_DTotal')[0].value='".$SUM["SUM"]."'</script>";
										}
										
										
									}
									else
									{
										echo"<tr><td colspan='6'>No Records Retrieved</td></tr>";
									}
								}
								else
								{
									$SQLColRecords="Select* from tbl_collection";
									$ColRecords=$con->query($SQLColRecords);
									if($ColRecords->num_rows>0)
									{
										while($ColRecord=$ColRecords->fetch_assoc())
										{
											echo"
											<tr>
												<td>".$ColRecord["Date"]."</td>
												<td>".$ColRecord["Hospital"]."</td>
												<td>".$ColRecord["ORNumber"]."</td>
												<td>".$ColRecord["checkAmt"]."</td>
												<td><button name='btn_Invoices'type='button'onClick='return show(this.value);'value='".$ColRecord["Invoice"]."'>Show Invoices</button></td>
												<td>".$ColRecord["CollectionOfficer"]."</td>
											</tr>
											";
										}
									}
									else
									{
										echo"<tr><td colspan='6'>No Records Retrieved</td></tr>";
									}
								}
								
							?>
						</tbody>
					</table>
						
					
				</div>
			</div>
		</div>
	</body>
</html>
